﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MouseHandler : MonoBehaviour
{
    // horizontal rotation speed
    public float mouseSensitivity = 100f;
    // vertical rotation speed
    //public float verticalSpeed = 100f;
    //private float xRotation = 0.0f;
    //private float yRotation = 0.0f;
    public Transform playerBody;
    float xRotation = 0f;
    float yRotation = 0f;

    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        yRotation += mouseX;
        xRotation += mouseY;
        //yRotation = Mathf.Clamp(yRotation, -90f, 90f);
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
        playerBody.Rotate(Vector3.up * mouseY);
        //playerBody.Rotate(Vector3.up * mouseY);
        /*yRotation += mouseX;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        Debug.Log(Camera.main);
        Camera.main.transform.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);*/
    }
}