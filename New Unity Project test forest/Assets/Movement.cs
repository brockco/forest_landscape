﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 12f;
    Camera cam;
    public Interactable focus;



    void Start()
    {
        cam = Camera.main;

    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * -x + transform.forward * -z;

        controller.Move(move * speed * Time.deltaTime);

        if (Input.GetMouseButtonDown(0))
        {
            //Interactable interactable = hit.collider.GetComponent<Interactable>();
            Destroy(GameObject.FindWithTag("interactable"));
        }
    }

    void SetFocus(Interactable newFocus)
    {
        focus = newFocus;
    }
}
