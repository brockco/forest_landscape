﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private int score;
    public string scoreText;

    void Start()
    {
        score = 0;
    }

    private void FixedUpdate()
    {
        score = score + 1;
        SetCountText();
    }

    void SetCountText()
    {
        scoreText = "Score: " + score.ToString();
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 0, 0), "Score:  " + score);
    }
}
