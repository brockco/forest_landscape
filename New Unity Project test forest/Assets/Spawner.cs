﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] obj;
    public float waitingForNextSpawn = 2;
    public float theCountdown = 2;
    public float xMin = -4;
    public float xMax = 4;
    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        theCountdown -= Time.deltaTime;
        if (theCountdown <= 0)
        {
            Spawn();
            theCountdown = waitingForNextSpawn;
        }
    }

    void Spawn()
    {
        Vector2 pos = new Vector2(Random.Range(xMin, xMax), 5);

        GameObject objPrefab = obj[Random.Range(0, obj.Length)];

        Instantiate(objPrefab, pos, transform.rotation);
    }
}
